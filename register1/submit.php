<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$facultate = "";
$sex = "";
$error = 0;


if(isset($_POST['firstname'])){
    $firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
    $lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
    $phone = $_POST['phone'];
}

if(isset($_POST['email'])){
    $email = $_POST['email'];
}

if(isset($_POST['cnp'])){
    $cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
    $facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
    $birth = $_POST['birth'];
}

if(isset($_POST['department'])){
    $department = $_POST['department'];
}

if(isset($_POST['question'])){
    $question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
    $captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
    $captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
    $check = $_POST['check'];
}

if(isset($_POST['facultate'])){
    $check = $_POST['facultate'];
}

if(isset($_POST['sex'])){
    $check = $_POST['sex'];
}

if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($cnp) || empty($facebook) || empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($captcha_generated) || empty($captcha_inserted) || empty($check) || empty($facultate)){
    $error = 1;
    $error_text ="One or more fields are empty!";
    }

if(strlen($firstname) < 3 || strlen($firstname) > 20){
    $error = 1;
    $error_text = "Firstname is shorter or longer than expacted!";
}

if(strlen($lastname) < 3 || strlen($lastname) > 20){
    $error = 1;
    $error_text = "Lastname is shorter or longer than expacted!";
}

if(stren($question) < 15){
    $error = 1;
    $error_text = "Question is not valid!";
}

if(!is_numeric($phone) || strlen($phone)!= 10){
    $error = 1;
    $error_text = "Phone number is not valid!";
}

if($i=0; &i<strlen($email); $i++){
    $arond=0;
    $pct=0;
    if($email[$i]=='@')$arond++;
    if($email[$i]=='.')$pct++;
    if($i==strlen($email) && ($arond!=1 || $pct!=1)){
        $error = 1;
        $error_text = "Email is not valid!";
    }
}

if(strlen($captcha_generated)== strlen($captcha_inserted)){
    for($i = 0;$i< = strlen($captcha_inserted);$i ++){
        if($captcha_generated[$i]!= $captcha_inserted[$i])
            $error = 1;
            $error_text = "Wrong captcha";
            break;
    }
}

if(strlen($cnp)!= 13 || ($cnp[0]!= 1 && $cnp[0]!= 2 && $cnp[0]!= 3 && $cnp[0]!= 4 && $cnp[0]!= 5 && $cnp[0]!= 6)){
    $error = 1;
    $error_text = "CNP is not valid!";
}

if(strlen($facultate) < 3 || strlen($facultate) >30){
    $error = 1;
    $error_text = "Facultate is not valid!";
}

if($birth[0]* 1000 + $birth[1]* 100 + $birth[2]* 10 + $birth[3]> 2001 || $birth[0]* 1000 + $birth[1]* 100 + $birth[2]* 10 + $birth[3]<= 1919){
    $error = 1;
    $error_text = "The age is not aalid!";
}

if($cnp[0]==1 || $cnp[0]==3 || $cnp[o]==5){
    $sex = M;
}

if($cnp[0]==2 || $cnp[o]==4 || $cnp[0]==6){
    $sex = F;
}

$link = "https://www.facebook.com/";
for($i= 1; $i< strlen($link); $i++);
    if($link[$i]!= $facebook[$i]){
        $error = 1;
        $error_text = "Facebook is not valid!";
    }

try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}
$query= $con -> prepare("SELECT 'email' FROM 'register2' WHERE 'email' = ?");{
    $query -> bindValue(1,$email);
    $query -> execute(); 
    if($query ->rowcount()!=0){
        $error = 1;
        $error_text = "Email is not valid!";

    }
}

$query2 = $con -> prepare("SELECT * FROM 'register2'");
$query2 -> execute();
if($query2 -> rowCount() >= 50 ){
    $error = 1;
    $error_text="Too many participants!";
}

$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,facebook,birth,department,question,facultate) VALUES(:firstname,:lastname,:phone,:email,:cnp,:facebook,:birth,:department,:question,:facultate)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);
$stmt2 -> bindParam(':facultate',$facultate);

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

}else{
    echo "Succes";
}