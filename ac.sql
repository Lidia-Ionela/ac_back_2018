-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2018 at 05:20 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ac`
--

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  `department` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id`, `firstname`, `lastname`, `phone`, `email`, `facebook`, `birthdate`, `department`) VALUES
(1, 'Andrei', 'Ion', '0729601114', 'ajgieg@gmail.com', 'wehewhweh', '2017-02-12', 'it'),
(2, 'Tecla', 'Lidia', '0721896612', 'lidia.tecla49@gmail.com', 'https://www.facebook.com/lidia.tecla', '1999-05-07', 'it'),
(3, 'Tecla', 'Lidia', '0721896612', 'lidia.tecla49@gmail.com', 'https://www.facebook.com/lidia.tecla', '1999-05-07', 'it'),
(4, 'Tecla', 'Lidia', '0721896612', 'lidia.tecla49@gmail.com', 'https://www.facebook.com/lidia.tecla', '1999-05-07', 'it'),
(5, 'Tecla', 'Lidia', '0721896612', 'lidia.tecla49@gmail.com', 'https://www.facebook.com/lidia.tecla', '1999-05-07', 'it'),
(6, 'Tecla', 'Lidia', '0721896612', 'lidia.tecla49@gmail.com', 'https://www.facebook.com/lidia.tecla', '1999-05-07', 'it'),
(7, 'Tecla', 'Lidia', '0721896612', 'lidia.tecla49@gmail.com', 'https://www.facebook.com/lidia.tecla', '1999-05-07', 'it'),
(8, 'Tecla', 'Lidia', '0721896612', 'lidia.tecla49@gmail.com', 'https://www.facebook.com/lidia.tecla', '1999-05-07', 'it'),
(9, 'Tecla', 'Lidia', '0721896612', 'lidia.tecla49@gmail.com', 'https://www.facebook.com/lidia.tecla', '1999-05-07', 'it'),
(10, 'Tecla', 'Lidia', '0721896612', 'lidia.tecla49@gmail.com', 'https://www.facebook.com/lidia.tecla', '1999-05-07', 'it'),
(11, 'Tecla', 'Lidia', '0721896612', 'lidia.tecla49@gmail.com', 'https://www.facebook.com/lidia.tecla', '1999-05-07', 'it'),
(12, 'Tecla', 'Lidia', '0721896612', 'lidia.tecla49@gmail.com', 'https://www.facebook.com/lidia.tecla', '1999-05-07', 'it'),
(13, 'Tecla', 'Lidia', '0721896612', 'lidia.tecla49@gmail.com', 'https://www.facebook.com/lidia.tecla', '1999-05-07', 'it'),
(14, 'Tecla', 'Lidia', '0721896612', 'lidia.tecla49@gmail.com', 'https://www.facebook.com/lidia.tecla', '1999-05-07', 'it'),
(15, 'Tecla', 'Lidia', '0721896612', 'lidia.tecla49@gmail.com', 'https://www.facebook.com/lidia.tecla', '1999-05-07', 'it'),
(16, 'Tecla', 'Lidia', '0721896612', 'lidia.tecla49@gmail.com', 'https://www.facebook.com/lidia.tecla', '1999-05-07', 'it');

-- --------------------------------------------------------

--
-- Table structure for table `register2`
--

CREATE TABLE `register2` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cnp` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `birth` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `question` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `register2`
--

INSERT INTO `register2` (`id`, `firstname`, `lastname`, `phone`, `email`, `cnp`, `facebook`, `birth`, `department`, `question`) VALUES
(1, 'Alexandru', 'Bisag', '0729601114', 'bisagalexstefan@gmail.com', '1234567891234', 'facebook.com/bisagalexstefan', '1997-07-28', 'it', 'Because');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `register2`
--
ALTER TABLE `register2`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `register2`
--
ALTER TABLE `register2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
